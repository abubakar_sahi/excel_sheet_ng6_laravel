import {Component, OnInit, HostListener} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {PartsService} from "../services/parts.service";

@Component({
  selector: 'app-parts',
  templateUrl: './parts.component.html',
  styleUrls: ['./parts.component.css']
})
export class PartsComponent implements OnInit {

    constructor(
        private toastr: ToastrService,
        private partService: PartsService)
    {}
    ngOnInit() {

    }
    parts = [];
    selectedPart;
    selectedCol = 'description';

    @HostListener('document:keyup', ['$event'])
    switchCol(event: KeyboardEvent) {
        if((event.ctrlKey || event.metaKey) && event.keyCode == 13){
            /*switch to next row (preserve column)**/
            this.moveToNextRow();
            console.log('CTRL + ENTER');
        }
        if((event.altKey) && event.keyCode == 13) {
            //switch to previous row (preserve column)
            this.moveToPreviousRow();
            console.log('ALT +  ENTER');
        }
        if((event.ctrlKey || event.metaKey) && event.keyCode == 46) {
            //delete current row
            console.log('CTRL +  del');
            this.DeleteSelectedRow();
        }
        if((event.ctrlKey || event.metaKey) && event.keyCode == 107) {
            //add new empty row
            this.addEmptyRow();
            console.log('CTRL +  +');
        }
    }

    createParts($event){
      var parts_data = $event.clipboardData.getData('Text');
      var parts_ar = parts_data.split('\n');

      for (var i = 0; i < parts_ar.length; i++) {

          var item = parts_ar[i];
          var parts_item = item.split('\t');
          if(parts_item.length < 2){
              parts_item = item.split(',');
          }
          if(parts_item.length > 4) {
              var found = this.searchInArray(parts_item[1]);
              if(found!=-1){
                  this.parts[found].qty +=parseInt(parts_item[3]);
              }else {
                  var part = {'description':'','part_no':'','brand_name':'','qty':0,'price':'','condition':'','location':''};
                  part.description = parts_item[0];
                  part.part_no = parts_item[1];
                  part.brand_name = parts_item[2];
                  part.qty = parseInt(parts_item[3]);
                  part.price = parts_item[4];
                  part.condition = parts_item[5];
                  part.location = parts_item[6];
                  this.parts.push(part);
              }
          }
      }
    }

    searchInArray(part_no){
      for (var i = 0; i < this.parts.length; i++) {
          if (this.parts[i].part_no.trim() === part_no.trim()) {

              return i;
          }
      }
      return -1;
    }

    isEditable(group_id){
        if(this.selectedPart==group_id) {
            return true;
        }else{
            return false;
        }
    }

    editGroupKey($event, col, index){
        this.parts[index][col] = $event.target.value;
    }

    saveData(){
        this.partService.createPart({'parts':this.parts}).subscribe(result=>{
            this.toastr.success(result.message, 'Success', {
                timeOut: 2500,
                positionClass: 'toast-bottom-center',
            });
        },error=>{
            this.toastr.error(error.message, 'Error', {
                timeOut: 2500,
                positionClass: 'toast-bottom-center',
            });
        })
    }

    addEmptyRow(){
        var part = {'description':'','part_no':'','brand_name':'','qty':0,'price':'','condition':'','location':''};
        part.description = '';
        part.part_no = '';
        part.brand_name = '';
        part.qty = 0;
        part.price = '';
        part.condition = '';
        part.location = '';
        this.parts.push(part);
    }

    DeleteSelectedRow(){

        this.parts.splice(this.selectedPart, 1);
    }

    moveToPreviousRow(){
        if(this.selectedPart){
            this.selectedPart = this.selectedPart -1;
        }
    }
    moveToNextRow(){
        if(this.selectedPart==undefined){
            this.selectedPart = 0;
        }
        this.selectedPart = this.selectedPart +1;
    }
}
