import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpHeaders} from "@angular/common/http";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment.prod";

@Injectable({
  providedIn: 'root'
})
export class PartsService {

  constructor(private httpClient:HttpClient) { }

  /**
   *
   * @param data
   * @returns {Observable<Object>}
   */
  createPart(Data):Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.httpClient.post(environment.apiUrl+'/parts', Data, httpOptions);
  }
}
