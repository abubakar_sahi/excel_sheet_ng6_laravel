<?php
/**
 * Created by PhpStorm.
 * User: fara
 * Date: 11/2/2017
 * Time: 5:14 PM
 */

namespace App\Repositories;


use App\Parts;

class PartsRepository implements PartsRepositoryInterface
{
    protected $model;
    protected $partsRepository;

    public function __construct(Parts $parts)
    {
        $this->model = $parts;

    }

    /**
     * @param $data
     * @return mixed
     */

    public function store($data)
    {
        $part_already_saved = $this->getPart($data['part_no']);
        if($part_already_saved) {
            $part_already_saved->qty += $data['qty'];
            return $part_already_saved->save();
        }
        return $this->model->create($data);
    }

    /**
     * @param $data
     * @return mixed
     */

    public function getPart($part_id)
    {
        return $this->model->where('part_no', $part_id)->first();
    }



}