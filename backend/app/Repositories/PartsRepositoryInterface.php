<?php
/**
 * Created by PhpStorm.
 * User: fara
 * Date: 11/2/2017
 * Time: 5:13 PM
 */

namespace App\Repositories;

interface PartsRepositoryInterface
{

    /**
     * @param $data
     * @return mixed
     */
    public function store($data);

    /**
     * @param $client_id
     * @return mixed
     */
    public function getPart($client_id);

}