<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parts extends Model
{
    //
    protected $table = 'parts';
    protected $fillable = ['brand_name','condition','location','part_no','price','qty','description'];
}
