<?php

namespace App\Providers;

use App\Repositories\PartsRepository;
use App\Repositories\PartsRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PartsRepositoryInterface::class, PartsRepository::class);
        //
    }
}
