<?php

namespace App\Http\Controllers;

use App\Parts;
use App\Repositories\PartsRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PartsController extends Controller
{
    protected $request;
    protected $partsRepository;
    /**
     *
     * @param Request $request
     * @param Product $user
     */
    public function __construct(Request $request,
        PartsRepositoryInterface $partsRepository
    ) {
        $this->partsRepository = $partsRepository;
        $this->request = $request;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->request->all();
//        $parts_model = new Parts;

        if(!empty($data['parts'])) {
            foreach ($data['parts'] as  $part){
                $this->partsRepository->store($part);
            }
        }

        if(Response::HTTP_CREATED==201){
            return response()->json(['status' => Response::HTTP_CREATED,'message'=>'Successfully Added to Database']);
        }else{
            return response()->json(['status' => Response::HTTP_CREATED,'message'=>'Some Error Occurred!'],503);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
